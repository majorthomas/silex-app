# Silex test project #

### What is this repository for? ###

* Test Silex micro-framework
* [Silex project](https://github.com/silexphp/Silex)

### How do I get set up? ###

In development environment use :

    composer install

In production environment use :

    composer install -o

*Optimize the autoloader. [Composer documentation](https://getcomposer.org/doc/03-cli.md#install)*