<?php

use Silex\Application;

use Symfony\Component\HttpFoundation\Request;

use App\Provider as Provider;
use App\Services as Services;

$rootdir = realpath(__DIR__ . '/..');

require_once $rootdir .'/vendor/autoload.php';

$app = new Application();
$app['rootdir'] = $rootdir;
unset($rootdir);

// Load the environment

$app['scalar.interpreter'] = $app->share(function() use ($app){
    return new Services\ScalarInterpreterService();
});

$app['environment'] = $app->share(function() use ($app){
    $dotenvloader = new Services\DotEnvLoaderService($app['rootdir'] .'/.env');

    return new Services\EnvironmentService(
        $dotenvloader->toArray(),
        $app['scalar.interpreter'],
        $app['rootdir']
    );
});

$app['debug'] = $app['environment']->getDebug();

$app['cache.max_age'] = 3600 * 20;
$app['cache.expires'] = $app['cache.max_age'];

// Default cache values
$app['cache.defaults'] = array(
    'Cache-Control'     => sprintf('public, max-age=%d, s-maxage=%d, must-revalidate, proxy-revalidate', $app['cache.max_age'], $app['cache.max_age']),
    'Expires'           => date('r', time() + $app['cache.expires']),
);

// Add a protected function in order to remove a non empty directory
$app['app.rrmdir'] = $app->protect(function($dir) use ($app) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir."/".$object) == "dir") $app['app.rrmdir']($dir."/".$object);
                else unlink($dir."/".$object);
            }
        }
        reset($objects);
        rmdir($dir);
    }
});

Request::setTrustedProxies(array('127.0.0.1', '::1'));

$app->register(new Provider\LowLevelServiceProvider());
$app->register(new Provider\ControllerProvider());

return $app['debug'] ? $app : $app['http_cache'];