<?php

namespace App\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;

class HomeController {

    protected $twig;

    public function __construct(Twig_Environment $twig) {
        $this->twig = $twig;
    }

    public function indexAction(Request $request, Application $app) {
        $template_name = 'index.twig';
        $cache_headers = $app['cache.defaults'];

        // Updates the Last-Modified HTTP header
        $cache_headers['Last-Modified'] = date('r', $app['twig.template_loader']($template_name));

        // Builds the response
        $response = $this->twig->render($template_name, array());

        // Sends the response
        return new Response($response, 200, $app['debug'] ? array() : $cache_headers);
    }
}