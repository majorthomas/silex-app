<?php

namespace App\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;

class CacheController {

    protected $twig;

    public function __construct(Twig_Environment $twig) {
        $this->twig = $twig;
    }

    public function indexAction(Request $request, Application $app) {
        try {
            $this->twig->clearCacheFiles();
            $this->twig->clearTemplateCache();

            $app['app.rrmdir']($app['http_cache.cache_dir']);

            $message = 'Cache cleared !';
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        return new Response($message, 200, array()); 
    }
}