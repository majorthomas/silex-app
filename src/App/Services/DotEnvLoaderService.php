<?php

namespace App\Services;

use josegonzalez\Dotenv\Loader as Loader;

class DotEnvLoaderService {

    protected $loader;

    public function __construct($envpath) {

        if ( !is_file($envpath) ) {
            throw new \InvalidArgumentException("Missing .env file", 1);
        }

        $this->loader = (new Loader($envpath))->parse();
    }

    public function toArray() {
        return $this->loader->toArray();
    }
}