<?php

namespace App\Services;

use Silex\Application;

use Symfony\Component\HttpFoundation\Request;

class EnvironmentService {

    protected $env;
    protected $rootdir;
    protected $appdir;
    protected $appsrcdir;
    protected $cachedir;

    protected $domain;
    protected $scheme;
    protected $siteurl;

    protected $debug;

    public function __construct(array $env, ScalarInterpreterService $scalarinterpreter, $rootdir) {
        $this->env = $env;
        $this->rootdir = $rootdir;
        $this->appdir = $this->rootdir .'/app';
        $this->appsrcdir = $this->rootdir .'/src';
        $this->cachedir = $this->appdir .'/cache';

        $rootrequest = Request::createFromGlobals();
        $this->domain = $rootrequest->getHost();
        $this->scheme = $rootrequest->getScheme();
        $this->siteurl = $this->scheme . '://' . $rootrequest->getHttpHost() . $rootrequest->getBaseUrl();

        $this->debug = $scalarinterpreter->toBooleanDefaultFalse($this->getEnv('DEBUG'));
    }

    public function getEnv($key) {
        return array_key_exists($key, $this->env) ? $this->env[$key] : null;
    }

    public function getRootDir() {
        return $this->rootdir;
    }

    public function getAppDir() {
        return $this->appdir;
    }

    public function getAppSrcDir() {
        return $this->appsrcdir;
    }

    public function getCacheDir() {
        return $this->cachedir;
    }

    public function getDomain() {
        return $this->domain;
    }

    public function getScheme() {
        return $this->scheme;
    }

    public function getSiteurl() {
        return $this->siteurl;
    }

    public function getDebug() {
        return $this->debug;
    }
}