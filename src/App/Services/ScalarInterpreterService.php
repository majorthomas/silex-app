<?php

namespace App\Services;

class ScalarInterpreterService {
    public function __construct() {
    }

    public function toBooleanDefaultFalse($value) {
        if(in_array(
            strtolower($value),
            array(TRUE, 'true', 'on', 'yes'),
            TRUE
        )) {
            return TRUE;
        }
        return FALSE;
    }

    public function toBooleanDefaultTrue($value) {
        if(in_array(
            strtolower($value),
            array(FALSE, 'false', 'off', 'no'),
            TRUE
        )) {
            return FALSE;
        }
        return TRUE;
    }
}