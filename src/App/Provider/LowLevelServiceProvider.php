<?php
namespace App\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Silex\Provider\HttpCacheServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;

class LowLevelServiceProvider implements ServiceProviderInterface {

    public function register(Application $app) {
        // Allows to use service name as controller in route definition
        $app->register(new ServiceControllerServiceProvider());

        // URL Generator service
        $app->register(new UrlGeneratorServiceProvider());

        // Twig service
        $app->register(new TwigServiceProvider(), array(
            'twig.path' => $app['environment']->getAppDir() .'/views/',
            'twig.options'  => array(
                'cache'     => $app['environment']->getCacheDir() .'/twig',
            )
        ));

        // Registers Symfony Cache component extension
        $app->register(new HttpCacheServiceProvider(), array(
            'http_cache.cache_dir' => $app['environment']->getCacheDir() .'/http',
        ));

        // Twig loader (handles last-mod file + re-compile file if not fresh)
        $app['twig.template_loader'] = $app->protect(function($template_name) use ($app) {

            // Returns immediately the current time when in debug mode
            if ($app['debug']) {
                return time();
            }

            // Gets the cache file and its modified time
            $cache = $app['twig']->getCacheFilename($template_name);
            $cache_time = is_file($cache) ? filemtime($cache) : 0;

            // If there is a newer version of the template
            if (false === $app['twig']->isTemplateFresh($template_name, $cache_time)) {

                // Deletes the cached file
                @unlink($cache);

                // Flushes the application HTTP cache for the current request
                $app['http_cache']->getStore()->invalidate($app['request']);

                // Returns the cache modified file time (as generated now)
                return time();
            }

            // Returns the template modified time
            return $cache_time;
        });
    }

    public function boot(Application $app) {

    }
}