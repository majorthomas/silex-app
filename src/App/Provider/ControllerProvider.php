<?php

namespace App\Provider;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ServiceProviderInterface;

use App\Controller as Controller;

class ControllerProvider implements ControllerProviderInterface, ServiceProviderInterface {

    public function register(Application $app) {
        $app['home.controller'] = $app->share(function() use ($app) {
            return new Controller\HomeController(
                $app['twig']
            );
        });

        $app['domain.controller'] = $app->share(function() use ($app) {
            return new Controller\DomainController(
                $app['twig']
            );
        });

        $app['cache.controller'] = $app->share(function() use ($app) {
            return new Controller\CacheController(
                $app['twig']
            );
        });
    }

    public function connect(Application $app) {
        // Creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        // Routing request

        // Homepage
        $controllers->get('/', 'home.controller:indexAction')
            ->bind('home');

        // Domain page
        $controllers->get('/domain/{name}', 'domain.controller:indexAction')
            ->assert('name', '^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$')
            ->bind('domain');

        // Test clearing cache via URL
        $controllers->get('/clearcache', 'cache.controller:indexAction')
            ->bind('clearcache');

        return $controllers;
    }

    public function boot(Application $app) {
        $app->mount('/', $this->connect($app));
    }

}